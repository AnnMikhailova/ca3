.data:
    string hw "Hello world!|"
    num cur_ind 0
.text:
    mov [hw] cur_ind
loop: out_rel cur_ind
    cmp* cur_ind 124
    je end
    jmp loop
end: hlt
