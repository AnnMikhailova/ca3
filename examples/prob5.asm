.data:
    num divisor 0
    num last 21
    num ost 0
    num ans 9699690

.text:

loop: mov 2 divisor

division: cmp last divisor
          je end
          mov 0 ost
          rdiv ans divisor
          sv ost
          cmp ost 0
          je noReminder
          add 9699690 ans
          jmp loop

noReminder: add 1 divisor
            jmp division

end:    out ans
        hlt
