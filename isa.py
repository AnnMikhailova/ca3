"""Типы данных для представления и сериализации/десериализации машинного кода."""
# !/usr/bin/python3
# pylint: disable=missing-function-docstring  # чтобы не быть Капитаном Очевидностью

import json
from enum import Enum


# Зарезервированные адреса, куда подключены потоки ввода-вывода
PRINT_ADDR: int = 100
READ_ADDR: int = 99


class Opcode(str, Enum):
    """Opcode для ISA."""

    HLT = 'hlt'
    MOV = 'mov'
    MOV_REL = 'mov_rel'
    MOV_CHAR = 'mov_char'
    CMP = 'cmp'
    CMP_REL_INC = 'cmp_rel_inc'
    RDIV = 'rdiv'
    ADD = 'add'
    INC = 'inc'
    JMP = 'jmp'
    JE = 'je'
    SV = 'sv'


inst_to_mc = {
    'hlt': [12],
    'mov': [0, 2, 10],
    'mov_rel': [0, 1, 9, 10],
    'mov_char': [0, 9, 10],
    'cmp': [0, 5, 10],
    'cmp_rel_inc': [0, 1, 5, 8, 4, 10],
    'rdiv': [0, 6, 10],
    'add': [0, 7, 2, 10],
    'jmp': [11],
    'je': [13],
    'sv': [3, 10]
}


def write_code(filename, code, data):
    with open(filename, "w", encoding="utf-8") as file:
        file.write(json.dumps(code, indent=4))

    with open("data_file", "w", encoding="utf-8") as file:
        file.write(json.dumps(data, indent=4))


def read_code(filename):
    with open(filename, encoding="utf-8") as file:
        code = json.loads(file.read())

    with open("data_file", encoding="utf-8") as file:
        data_section = json.loads(file.read())

    return code, data_section
