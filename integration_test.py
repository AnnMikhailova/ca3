# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=line-too-long
# pylint: disable=trailing-whitespace
# pylint: disable=useless-import-alias
# pylint: disable=missing-module-docstring

import contextlib
import io
import logging
import os
import tempfile
import unittest
import pytest as pytest
import translator
import machine


@pytest.mark.golden_test("examples/*.yml")
def test_whole_by_golden(golden, caplog):
    caplog.set_level(logging.DEBUG)

    with tempfile.TemporaryDirectory() as tmpdirname:
        source_file = os.path.join(tmpdirname, "example.asm")
        target = os.path.join(tmpdirname, "example.json")
        input_file = os.path.join(tmpdirname, "input.txt")

        with open(source_file, "w", encoding="utf-8") as file:
            file.write(golden["source"])
        with open(input_file, "w", encoding="utf-8") as file:
            file.write(golden["input"])

        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            translator.main([source_file, target])
            machine.main([target, input_file])

        with open(target, encoding="utf-8") as file:
            code = file.read()

        assert code == golden.out["code"]
        assert stdout.getvalue() == golden.out["output"]
        assert caplog.text == golden.out["log"]



class TestCases(unittest.TestCase):

    def test_cat(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "examples/cat.asm"
            target = os.path.join(tmpdirname, "cat.json")
            input_stream = "examples/input.txt"
            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, input_stream])
                self.assertEqual(stdout.getvalue(),
                                 'source LoC: 14 code instr: 4\n'
                                 'Good news, everyone!\n\n'
                                 'instr_counter: 63 ticks: 147\n'
                                 )

    def test_hello(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "examples/hello.asm"
            target = os.path.join(tmpdirname, "hello.json")
            input_stream = "examples/input.txt"
            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, input_stream])
            self.assertEqual(stdout.getvalue(),
                             'source LoC: 24 code instr: 6\n'
                             'Hello world!|\n'
                             'instr_counter: 52 ticks: 158\n'
                             )

    def test_prop5(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            source = "examples/prob5.asm"
            target = os.path.join(tmpdirname, "prob5.json")
            input_stream = "examples/input.txt"

            with contextlib.redirect_stdout(io.StringIO()) as stdout:
                translator.main([source, target])
                machine.main([target, input_stream])
                self.assertEqual(stdout.getvalue(),
                                 'source LoC: 52 code instr: 14\n'
                                 '232792560\n'
                                 'instr_counter: 1323 ticks: 3103\n'
                                 )
